import django.forms as forms
from models import Post

class PostForm(forms.ModelForm):
    class Meta:
    	model = Post
    	fields = ('titulo','corpo')

    def __init__(self, *args, **kwargs):
        self.base_fields['corpo'].widget = forms.Textarea()
        super(PostForm, self).__init__(*args, **kwargs)

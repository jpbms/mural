from forms import PostForm
from models import Post

from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required

@login_required
def postit(request):

	all_posts = Post.objects.filter(aprovado=True)
	print all_posts.count()
	print all_posts
	if request.method == 'POST':
		form = PostForm(request.POST)
		if form.is_valid():
			form.instance.user = request.user
			form.save()
			form = PostForm()
	else:
		form = PostForm()
	return render_to_response('postit.html', locals(), context_instance=RequestContext(request),)

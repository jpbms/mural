angular.module('postit',['ngResource'])
   .factory('Posts',['$resource', function($resource) {
      return $resource('api/posted/?format=json', {}, {
      	query: {method: 'GET', isArray:true}
      });	
   }])
   .controller('ListPostsCtrl', function($scope, $rootScope, $timeout, Posts) {

   		$scope.all_posts = Posts.query();

   		var updateList = function() {
			$scope.all_posts = Posts.query();
			$timeout(updateList,5000,true);   			
   		}
   		$timeout(updateList,5000,true);

   })
   .controller('PostagemCtrl', function($scope, $rootScope) {
      $scope.texto = ''
      $scope.caracteres = 0;

      $scope.$watch("texto", function(newVal, oldVal, scope) {
      	scope.caracteres = newVal.length;
      })
   });
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Post(models.Model):
	titulo = models.CharField(max_length=40)
	corpo = models.CharField(max_length=140)
	user = models.ForeignKey(User)
	aprovado = models.BooleanField(default=False)

	def __unicode__(self):
		return self.titulo

from models import Post
from django.contrib.auth.models import User
from rest_framework import serializers, generics

class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')
        read_only_fields = ('id', 'username')

class PostSerializers(serializers.ModelSerializer):
    class Meta:
        model = Post
        depth = 2
        fields = ('id', 'titulo', 'corpo', 'user','aprovado')
        read_only_fields = ('user','aprovado')


    user = UserSerializers()


class PostList(generics.ListCreateAPIView):
    model = Post
    serializer_class = PostSerializers
    
    def get_queryset(self):
    	return Post.objects.filter(aprovado=True)

class PostDetails(generics.RetrieveUpdateDestroyAPIView):
    model = Post
    serializer_class = PostSerializers

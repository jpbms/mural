from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns

from api import PostList, PostDetails

urlpatterns = patterns('sgp_assessment.views',

	url(r'^posted/$', PostList.as_view(), name='post_list_endpoint'),
	url(r'^posted/(?P<pk>[^/]+)/$', PostDetails.as_view() , name='post_details_endpoint'),

)
